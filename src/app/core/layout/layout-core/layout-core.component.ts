import {Component, Input} from '@angular/core';
import {list} from "postcss";

@Component({
  selector: 'app-layout-core',
  template: `
      <nav class="navbar navbar-expand-lg navbar-light bg-light px-4">
          <a class="navbar-brand" href="#" (click)="switchDisplay()">Super Hero Management Application</a>
          <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                  <a class="nav-link"
                     [routerLink]="['/', 'super-heroes', 'list']"
                      routerLinkActive="active"
                     href="#">Liste des héros</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link"
                     [routerLink]="['/', 'powers', 'list']"
                      routerLinkActive="active"
                     href="#">Liste des pouvoirs</a>
              </li>
          </ul>
      </nav>

      <div class="mx-2">
          <router-outlet></router-outlet>
      </div>
  `,
  styles: `
  .active {
    color: red !important;
    }`
})
export class LayoutCoreComponent {

  @Input() title = 'Super Project';

  displayedPage: 'list' | 'details' | 'home' = 'home';

  switchDisplay(page: 'list' | 'details' | 'home' = 'home'): void {
    this.displayedPage = page;
  }

  protected readonly list = list;
}
