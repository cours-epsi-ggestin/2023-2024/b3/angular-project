import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutCoreComponent} from './layout/layout-core/layout-core.component';
import {SuperHeroModule} from "../super-hero/super-hero.module";
import {CoreRoutingModule} from "./core-routing.module";


@NgModule({
  declarations: [
    LayoutCoreComponent
  ],
  exports: [
    LayoutCoreComponent
  ],
  imports: [
    CommonModule,
    SuperHeroModule,
    CoreRoutingModule
  ]
})
export class CoreModule {
}
