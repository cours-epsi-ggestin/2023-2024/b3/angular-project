import {Component, Input, OnInit, signal, WritableSignal} from '@angular/core';
import {PowerService} from "../../services/power.service";
import {Power} from "../../models/power";
import {Router} from "@angular/router";
import {PowerForm} from "../../models/powerForm";

@Component({
  selector: 'app-power-edit',
  template: `
    @if (power() === null || loading()) {
        <span>Loading...</span>
    } @else {
      <app-power-form [power]="power()!" (validate)="updatePower($event)"></app-power-form>
    }
  `
})
export class PowerEditComponent implements OnInit {
  @Input() id!: number;

  power: WritableSignal<Power | null> = signal(null);
  loading = this.powerService.loading;

  constructor(
    private router: Router,
    private powerService: PowerService
  ) {
  }

  ngOnInit(): void {
    this.powerService.getPower(this.id).subscribe(power => {
      this.power.set(power);
    })
  }

  updatePower(power: PowerForm) {
    this.powerService.updatePower(this.id, power).subscribe(power => {
      this.router.navigate(['/powers'])
    })
  }

}
