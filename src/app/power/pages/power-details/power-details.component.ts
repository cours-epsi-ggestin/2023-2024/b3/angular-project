import {Component, computed, Input, OnInit, Signal, signal, WritableSignal} from '@angular/core';
import {PowerService} from "../../services/power.service";
import {Power} from "../../models/power";
import {catchError, of} from "rxjs";

@Component({
  selector: 'app-power-details',
  templateUrl: './power-details.component.html'
})
export class PowerDetailsComponent implements OnInit {
  @Input() id!: number;

  power: WritableSignal<Power | null> = signal(null);
  loading = signal(false);

  displayState: Signal<'loading' | 'display' | 'error'> = computed(() => {
    if (this.loading()) {
      return 'loading';
    }

    return this.power() === null ? 'error' : 'display';
  });

  constructor(
    private readonly powerService: PowerService,
  ) {
  }

  ngOnInit(): void {
    this.loading.set(true)
    this.powerService.getPower(this.id)
      .pipe(
        catchError(() => of(null))
      )
      .subscribe(power => {
        this.power.set(power);
        this.loading.set(false);
      });
  /*this.powerService.getPower(this.id)
      .pipe(
        // catchError(() => of(null))
      )
      .subscribe({
        next: power => {
          this.power.set(power);
          this.loading.set(false);
        },
        error: () => {
          this.power.set(null);
          this.loading.set(false);
        }
      });*/
  }
}
