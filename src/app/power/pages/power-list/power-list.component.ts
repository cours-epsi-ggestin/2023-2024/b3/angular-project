import {Component, OnInit, signal} from '@angular/core';
import {PowerService} from "../../services/power.service";
import {switchMap, tap} from "rxjs";
import {PageEvent} from "../../../shared/pagination/pagination.component";

@Component({
  selector: 'app-power-list',
  templateUrl: './power-list.component.html'
})
export class PowerListComponent implements OnInit {

  firstElement = signal(0);
  lastElement = signal(0);
  totalCount = signal(0);

  powers$ = this.powerService.powers$.pipe(
    tap(powers => this.totalCount.set(powers.length))
  );

  constructor(
    private powerService: PowerService
  ) {
  }

  ngOnInit(): void {
    this.powerService.getPowers().subscribe();
  }

  deletePower($event: MouseEvent, id: number) {
    $event.stopPropagation();
    this.powerService.deletePower(id)
      .pipe(
        switchMap(() => this.powerService.getPowers())
      )
      .subscribe();
  }

  pageChange(pageEvent: PageEvent) {
    this.firstElement.set(pageEvent.first);
    this.lastElement.set(pageEvent.last);
  }
}
