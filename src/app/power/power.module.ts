import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PowerRoutingModule } from './power-routing.module';
import { LayoutPowerComponent } from './layout/layout-power/layout-power.component';
import { PowerListComponent } from './pages/power-list/power-list.component';
import { PowerDetailsComponent } from './pages/power-details/power-details.component';
import { PowerCreateComponent } from './pages/power-create/power-create.component';
import { PowerEditComponent } from './pages/power-edit/power-edit.component';
import { PowerFormComponent } from './components/power-form/power-form.component';
import {PaginationComponent} from "../shared/pagination/pagination.component";
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    LayoutPowerComponent,
    PowerListComponent,
    PowerDetailsComponent,
    PowerCreateComponent,
    PowerEditComponent,
    PowerFormComponent
  ],
    imports: [
        CommonModule,
        PowerRoutingModule,
        PaginationComponent,
        FormsModule
    ]
})
export class PowerModule { }
