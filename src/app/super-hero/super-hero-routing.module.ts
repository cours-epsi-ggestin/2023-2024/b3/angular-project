import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SuperHeroListComponent} from "./pages/super-hero-list/super-hero-list.component";
import {SuperHeroDetailsComponent} from "./pages/super-hero-details/super-hero-details.component";
import {LayoutPowerComponent} from "../power/layout/layout-power/layout-power.component";
import {LayoutSuperHeroComponent} from "./layout/layout-super-hero/layout-super-hero.component";

const routes: Routes = [{
  path: '',
  component: LayoutSuperHeroComponent,
  children: [
    { path: 'list', component: SuperHeroListComponent },
    { path: ':id/details', component: SuperHeroDetailsComponent }
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperHeroRoutingModule {
}
