import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutSuperHeroComponent } from './layout-super-hero.component';

describe('LayoutSuperHeroComponent', () => {
  let component: LayoutSuperHeroComponent;
  let fixture: ComponentFixture<LayoutSuperHeroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LayoutSuperHeroComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(LayoutSuperHeroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
