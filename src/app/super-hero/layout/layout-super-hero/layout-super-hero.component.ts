import { Component } from '@angular/core';

@Component({
  selector: 'app-layout-super-hero',
  template: `
      <div class="d-flex flex-column gap-4 pt-4 px-5">
          <h1>Super Hero</h1>
          <div class="d-flex gap-2 px-4">
              <a class="btn btn-primary" [routerLink]="['/', 'super-heroes', 'list']" routerLinkActive="active">Super Hero List</a>
          </div>
          <div>
              <router-outlet></router-outlet>
          </div>
      </div>
  `
})
export class LayoutSuperHeroComponent {

}
