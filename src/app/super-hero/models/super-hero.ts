export interface SuperHero {
  nameSuperHero: string;
  id: number;
  alterEgo: string;
}
