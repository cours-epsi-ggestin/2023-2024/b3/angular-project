import {Component, computed, DestroyRef, inject, OnDestroy, OnInit, signal, WritableSignal} from '@angular/core';
import {SuperHero} from "../../models/super-hero";
import {SuperHeroService} from "../../services/super-hero.service";
import {takeUntilDestroyed} from "@angular/core/rxjs-interop";
import {tap} from "rxjs";
import {PageEvent} from "../../../shared/pagination/pagination.component";

@Component({
  selector: 'app-super-hero-list',
  templateUrl: './super-hero-list.component.html'
})
export class SuperHeroListComponent implements OnInit {

  firstElement = signal(0);
  lastElement = signal(0);
  totalCount = signal(0);

  superHeros = this.superHeroService.listSuperHero$
    .pipe(
      tap((next) => this.totalCount.set(next.length)),
    )

  constructor(
    private superHeroService: SuperHeroService
  ) {
  }

  ngOnInit(): void {
    this.superHeroService.getSuperHeroes().subscribe();
  }

  pageChange(pageEvent: PageEvent) {
    this.firstElement.set(pageEvent.first);
    this.lastElement.set(pageEvent.last);
  }
}
