import {Component, computed, EventEmitter, Input, OnInit, Output, signal, WritableSignal} from '@angular/core';
import {NgClass, NgForOf} from "@angular/common";

export interface PageEvent {
  page: number;
  pageSize: number;
  first: number;
  last: number;
}

@Component({
  selector: 'app-pagination',
  standalone: true,
  imports: [
    NgForOf,
    NgClass
  ],
  template: `
      <p>page: {{ page() + 1 }}/{{ totalPage() }}</p>

      <div class="d-flex flex-row gap-4 align-items-center ">
          <nav aria-label="Page navigation example">
              <ul class="pagination m-0">
                  <li class="page-item cursor-pointer" [ngClass]="{disabled: isFirstPage()}">
                      <span class="page-link" (click)="firstPage()">First</span>
                  </li>
                  <li class="page-item cursor-pointer" [ngClass]="{disabled: isFirstPage()}">
                      <span class="page-link" (click)="previousPage()">Previous</span>
                  </li>
                  <li class="page-item cursor-pointer" [ngClass]="{disabled: isLastPage()}">
                      <span class="page-link" (click)="nextPage()">Next</span>
                  </li>
                  <li class="page-item cursor-pointer" [ngClass]="{disabled: isLastPage()}">
                      <span class="page-link" (click)="lastPage()">Last</span>
                  </li>
              </ul>
          </nav>

          <select class="form-select" (change)="changePageSize($event)">
              @for (pageSize of optionPageSize; track pageSize) {
                  <option [value]="pageSize" [selected]="pageSize == selectedPageSize()">
                      {{ pageSize }}
                  </option>
              }
          </select>
      </div>
  `
})
export class PaginationComponent implements OnInit {
  @Output() pageChange: EventEmitter<PageEvent> = new EventEmitter<PageEvent>();

  @Input() optionPageSize = [4, 8, 12, 16, 20]
  @Input() totalCount = signal(0)
  totalPage = computed(() => Math.ceil(this.totalCount() / this.selectedPageSize()))

  page: WritableSignal<number> = signal(0);
  selectedPageSize: WritableSignal<number> = signal(this.optionPageSize[0]);

  firstElement = computed(() => this.page() * this.selectedPageSize())

  lastElement = computed(() => this.firstElement() + this.selectedPageSize())
  isLastPage = computed(() => this.lastElement() >= this.totalCount())

  isFirstPage = computed(() => this.firstElement() < 1)
  private updatePage(event: Partial<PageEvent>) {
    if (event.page !== undefined) {
      this.page.set(event.page);
    }

    if (event.pageSize !== undefined) {
      this.selectedPageSize.set(event.pageSize);
    }

    if (event.page !== undefined || event.pageSize !== undefined) {
      this.pageChange.emit({
        page: this.page(),
        pageSize: this.selectedPageSize(),
        first: this.firstElement(),
        last: this.lastElement()
      });
    }
  }



  ngOnInit(): void {
    this.pageChange.emit({
      page: this.page(),
      pageSize: this.selectedPageSize(),
      first: this.firstElement(),
      last: this.lastElement()
    });
  }

  lastPage() {
    this.updatePage({page: this.totalPage() - 1});
  }

  nextPage() {
    this.updatePage({page: this.page() + 1});
  }

  firstPage() {
    this.updatePage({page: 0});
  }

  previousPage() {
    this.updatePage({page: this.page() - 1});
  }

  changePageSize(event: Event) {
    this.updatePage({pageSize: parseInt((event.target as HTMLSelectElement).value)});
  }

}
